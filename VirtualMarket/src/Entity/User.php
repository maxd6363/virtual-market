<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends Account
{
    const USER_DEFAULT_MONEY = 500.0;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\Length(min="3", max="32", minMessage="Votre prénom doit faire au minimum 3 caractères.", maxMessage="Votre prénom doit faire au maximum 32 caractères.")
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\Length(min="3", max="32", minMessage="Votre nom doit faire au minimum 3 caractères.", maxMessage="Votre nom doit faire au maximum 32 caractères.")
     */
    protected $lastName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
