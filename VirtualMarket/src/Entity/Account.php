<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="disc", type="string")
 * @ORM\DiscriminatorMap({"user" = "User", "company" = "Company"})
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 * @UniqueEntity(fields={"username"}, message="Un compte existe déjà avec ce nom d'utilisateur.")
 */
abstract class Account implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\Length(min="3", max="32", minMessage="Votre nom d'utilisateur doit faire au minimum 3 caractères.", maxMessage="Votre nom d'utilisateur doit faire au maximum 32 caractères.")
     */
    protected $username;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", max="32", minMessage="Votre mot de passe doit faire au minimum 8 caractères.", maxMessage="Votre mot de passe doit faire au maximum 255 caractères.")
     */
    protected $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="La confirmation du mot de passe est incorrecte.")
     */
    protected $confirmPassword;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Email(message="Vous devez entrer une adresse email valide.")
     */
    protected $email;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    protected $money;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Command", mappedBy="author")
     */
    protected $commandes;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function __construct()
    {
        $this->commandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @see UserInterface
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getMoney(): ?string
    {
        return $this->money;
    }

    public function setMoney(string $money): self
    {
        $this->money = $money;

        return $this;
    }

    /**
     * @return Collection|Command[]
     * @see UserInterface
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Command $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setAuthor($this);
        }

        return $this;
    }

    public function removeCommande(Command $commande): self
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getAuthor() === $this) {
                $commande->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // Not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->confirmPassword = null;
    }
}
