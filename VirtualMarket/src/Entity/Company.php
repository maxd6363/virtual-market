<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company extends Account
{
    const COMPANY_DEFAULT_MONEY = 0.0;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\Length(min="3", max="32", minMessage="Votre nom d'entreprise doit faire au minimum 3 caractères.", maxMessage="Votre nom d'entreprise doit faire au maximum 32 caractères.")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=2000)
     * @Assert\NotNull(message="Vous devez entrer une description pour votre entreprise.")
     * @Assert\Length(max="2000", maxMessage="La description de votre entreprise doit faire au maximum 2000 caractères.")
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Connection", mappedBy="company")
     */
    protected $connections;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="companies")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="company")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Upgrade", mappedBy="company")
     */
    private $upgrades;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    public function __construct()
    {
        parent::__construct();
        $this->connections = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->upgrades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Connection[]
     */
    public function getConnections(): Collection
    {
        return $this->connections;
    }

    public function addConnection(Connection $connection): self
    {
        if (!$this->connections->contains($connection)) {
            $this->connections[] = $connection;
            $connection->setCompany($this);
        }

        return $this;
    }

    public function removeConnection(Connection $connection): self
    {
        if ($this->connections->contains($connection)) {
            $this->connections->removeElement($connection);
            // set the owning side to null (unless already changed)
            if ($connection->getCompany() === $this) {
                $connection->setCompany(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setCompany($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getCompany() === $this) {
                $article->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Upgrade[]
     */
    public function getUpgrades(): Collection
    {
        return $this->upgrades;
    }

    public function addUpgrade(Upgrade $upgrade): self
    {
        if (!$this->upgrades->contains($upgrade)) {
            $this->upgrades[] = $upgrade;
            $upgrade->setCompany($this);
        }

        return $this;
    }

    public function removeUpgrade(Upgrade $upgrade): self
    {
        if ($this->upgrades->contains($upgrade)) {
            $this->upgrades->removeElement($upgrade);
            // set the owning side to null (unless already changed)
            if ($upgrade->getCompany() === $this) {
                $upgrade->setCompany(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
