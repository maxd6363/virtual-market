<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200201162942 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE account (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(32) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(100) DEFAULT NULL, money NUMERIC(10, 2) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , disc VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE article (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(64) NOT NULL, description VARCHAR(2000) NOT NULL, image VARCHAR(255) DEFAULT NULL, unit_price NUMERIC(10, 2) NOT NULL, company_id INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(64) NOT NULL, description VARCHAR(2000) NOT NULL, owner_id INTEGER DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C17E3C61F9 ON category (owner_id)');
        $this->addSql('CREATE TABLE command (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date DATETIME NOT NULL, quantity NUMERIC(3, 0) NOT NULL, total_price NUMERIC(10, 2) NOT NULL, article_id INTEGER NOT NULL, author_id INTEGER NOT NULL, disc VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE company (id INTEGER NOT NULL, name VARCHAR(32) NOT NULL, description VARCHAR(2000) NOT NULL, image VARCHAR(255) DEFAULT NULL, category_id INTEGER DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE connection (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date DATE NOT NULL, company_id INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE upgrade (id INTEGER NOT NULL, content VARCHAR(255) NOT NULL, end_date DATETIME DEFAULT NULL, company_id INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user (id INTEGER NOT NULL, first_name VARCHAR(32) NOT NULL, last_name VARCHAR(32) NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE command');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE connection');
        $this->addSql('DROP TABLE upgrade');
        $this->addSql('DROP TABLE user');
    }
}
