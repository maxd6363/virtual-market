<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Company;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;

class CompanyRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email', EmailType::class)
            ->add('name')
            ->add('password', PasswordType::class)
            ->add('confirmPassword', PasswordType::class)
            ->add('description')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name'
            ])
            ->add('imageFile', FileType::class, [
                'label' => 'Bannière (Doit être une image de taille 728x90)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Image([
                        'maxSize' => '1024k',
                        'minHeight' => 90,
                        'maxHeight' => 90,
                        'minWidth' => 728,
                        'maxWidth' => 728,
                        'mimeTypesMessage' => 'Veuillez donner une image dans un format valide.',
                        'maxSizeMessage' => 'La bannière ne doit pas faire plus de 1 Mb.',
                        'minHeightMessage' => 'La bannière doit faire une taille 728x90 pixels.',
                        'maxHeightMessage' => 'La bannière doit faire une taille 728x90 pixels.',
                        'minWidthMessage' => 'La bannière doit faire une taille 728x90 pixels.',
                        'maxWidthMessage' => 'La bannière doit faire une taille 728x90 pixels.',
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
