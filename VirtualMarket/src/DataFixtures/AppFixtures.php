<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Command;
use App\Entity\Company;
use App\Entity\Connection;
use App\Entity\Upgrade;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $categories = new ArrayCollection();
        $companies = new ArrayCollection();
        $articles = new ArrayCollection();
        $users = new ArrayCollection();

        // Category fixtures
        for ($i = 1; $i <= 5; $i++)
        {
            $category = new Category();
            $category->setName($faker->word())
                     ->setDescription($faker->paragraph())
                     ->setOwner(null);
            $manager->persist($category);
            $categories->add($category);
        }

        // Company fixtures (without persist)
        for ($i = 1; $i <= 12; $i++)
        {
            $company = new Company();
            $company->setUsername($faker->firstName())
                    ->setName($faker->name())
                    ->setRoles(['ROLE_COMPANY'])
                    ->setMoney(Company::COMPANY_DEFAULT_MONEY)
                    ->setEmail($faker->companyEmail)
                    ->setDescription($faker->paragraph())
                    ->setCategory($categories->get(rand(0, $categories->count() - 1)))
                    ->setPassword($this->passwordEncoder->encodePassword($company, 'password'))
                    ->setConfirmPassword($company->getPassword())
                    ->setImage(null);

            // Article fixtures
            $draw = 5;
            if($i > 1)
            {
                $draw = rand(0, 4);
            }
            for ($j = 1; $j <= $draw; $j++)
            {
                $article = new Article();
                $article->setCompany($company)
                    ->setName($faker->sentence(3))
                    ->setDescription($faker->paragraph())
                    ->setUnitPrice($faker->numberBetween(1, 100))
                    ->setImage(null);
                $company->addArticle($article);
                $manager->persist($article);
                $articles->add($article);
            }

            // Connection fixtures
            for ($j = 1; $j <= rand(5, 10); $j++)
            {
                $connection = new Connection();
                $connection->setCompany($company)
                    ->setDate($faker->dateTimeBetween('-1 years'));
                $company->addConnection($connection);
                $manager->persist($connection);
            }

            // Upgrade fixtures
            for ($j = 1; $j <= rand(0, 3); $j++)
            {
                if($i == 1)
                {
                    break;
                }
                $upgrade = new Upgrade();
                $upgrade->setCompany($company)
                    ->setEndDate($faker->dateTimeBetween('now', '+1 years'))
                    ->setDate($faker->dateTimeBetween('-1 years'))
                    ->setArticle($companies->first()->getArticles()->first())
                    ->setQuantity(1)
                    ->setTotalPrice($faker->numberBetween(1, 100))
                    ->setAuthor($company)
                    ->setContent($faker->sentence(3));
                $company->addUpgrade($upgrade);
                $manager->persist($upgrade);
            }
            $companies->add($company);
        }

        // User fixtures
        for ($i = 1; $i <= 20; $i++)
        {
            $user = new User();
            $user->setUsername($faker->firstName())
                ->setFirstName($faker->firstName())
                ->setLastName($faker->firstName())
                ->setRoles(['ROLE_USER'])
                ->setMoney(User::USER_DEFAULT_MONEY)
                ->setEmail($faker->email)
                ->setPassword($this->passwordEncoder->encodePassword($user, 'password'))
                ->setConfirmPassword($user->getPassword());

            // Command fixtures (1/2)
            for ($j = 1; $j <= rand(0, 6); $j++)
            {
                $command = new Command();
                $command->setAuthor($user)
                    ->setQuantity(rand(1, 3))
                    ->setArticle($articles->get(rand(0, $articles->count() - 1)))
                    ->setTotalPrice($command->getArticle()->getUnitPrice() * $command->getQuantity())
                    ->setDate($faker->dateTimeBetween('-1 years'));
                $user->addCommande($command);
                $manager->persist($command);
            }
            $users->add($user);
            $manager->persist($user);
        }

        // Command fixture (2/2) + persist companies
        foreach ($companies as $company) {
            for ($j = 1; $j <= rand(0, 8); $j++)
            {
                $command = new Command();
                $command->setAuthor($company)
                    ->setQuantity(rand(1, 3))
                    ->setArticle($articles->get(rand(0, $articles->count() - 1)))
                    ->setTotalPrice($command->getArticle()->getUnitPrice() * $command->getQuantity())
                    ->setDate($faker->dateTimeBetween('-1 years'));
                $company->addCommande($command);
                $manager->persist($command);
            }
            $manager->persist($company);
        }

        $manager->flush();
    }
}
